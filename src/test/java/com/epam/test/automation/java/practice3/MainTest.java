package com.epam.test.automation.java.practice3;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class MainTest {
    @DataProvider(name = "testTask1")
    public Object[][] dataProviderMethod1(){
        return new Object[][] { {new int[]{100, 2, 3, 45, 33, 8, 4, 54}, new int[]{54, 4, 3, 45, 33, 8, 2, 100}},
                {new int[]{10, 5, 3, 4}, new int[]{4, 5, 3, 10}},
                {new int[]{3, 5, 7, 9, 11, 13, 15, 17}, new int[]{3, 5, 7, 9, 11, 13, 15, 17}}};
    }
    @Test(dataProvider = "testTask1")
    public void testTask1(int[] array, int[] expected){
        var actual = Main.task1(array);
        Assert.assertEquals(actual, expected, "The method task1 works incorrectly");
    }
    @DataProvider(name = "testTask2")
    public Object[][] dataProvideMethod2(){
        return new Object[][]{{new int[]{100}, 0},
                {new int[]{100, 100}, 1},
                {new int[]{5, 350, 350, 4, 350}, 3},
                {new int[]{10, 10, 10, 10, 10}, 4}};
    }
    @Test(dataProvider = "testTask2")
    public void testTask2(int[] array, int expected){
        var actual = Main.task2(array);
        Assert.assertEquals(actual, expected, "The method task2 works incorrectly");
    }

    @DataProvider(name = "testTask3")
    public Object[][] dataProvideMethod3(){
        return new Object[][]{{new int[][]{{2, 4, 3, 3},{5, 7, 8, 5}, {2, 4, 3, 3}, {5, 7, 8, 5}}, new int[][]{{2, 1, 1, 1}, {0, 7, 1, 1}, {0, 0, 3, 1}, {0, 0, 0, 5}}}};
    }

    @Test(dataProvider = "testTask3")
    public void testTask3(int[][] matrix, int[][] expected){
        var actual = Main.task3(matrix);
        Assert.assertEquals(actual, expected, "The method task3 works incorrectly");
    }

}