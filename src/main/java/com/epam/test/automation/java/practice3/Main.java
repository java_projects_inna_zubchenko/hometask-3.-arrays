package com.epam.test.automation.java.practice3;

public class Main {
    private Main(){

    }

    public static int[] task1(int[] array) {
        int length = array.length;
        int temp;
        int i = 0;
        int j = length - 1;
        while (i <= length / 2 && j > i)
        {
            if (array[i] % 2 == 0 && array[j] % 2 == 0)
            {
                temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
            i++;
            j--;
        }
        return array;
    }

    public static int task2(int[] array) {
       int firstNumber = 0;
       int lastNumber = 0;
        for(int i = 0; i<array.length; i++){
            if (array[i]>array[firstNumber]){
                firstNumber = i;
            }
            if (array[i]>=array[lastNumber]){
                lastNumber = i;
            }
        }
       return lastNumber - firstNumber;
    }

    public static int[][] task3(int[][] matrix) {
     for(int i = 1; i< matrix.length; i++)
         for (int j = 0; j<i; j++){
             matrix[i][j] = 0;
             matrix[j][i]= 1;
         }
     return matrix;
    }

}
